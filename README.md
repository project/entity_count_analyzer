## Entity Count Analyzer

The Entity Count Analyzer module provides powerful reports to analyze and display the count of various entities within your Drupal site. It offers valuable insights into the content, users, blocks, and taxonomy terms, allowing you to understand the distribution and volume of these entities.

### Features

- **Node Count Report:** View a comprehensive report displaying the count of nodes based on content types, including the number of published and unpublished nodes. Access the report at `/admin/reports/node-type-count`.

- **User Count Report:** Gain insights into user demographics by exploring the count of users assigned to different roles. Identify role-based user distribution easily. Access the report at `/admin/reports/node-type-count/user`.

- **Block Count Report:** Analyze the count of blocks for different block types, enabling you to understand block usage and popularity across your site. Access the report at `/admin/reports/node-type-count/block`.

- **Term Count Report:** Explore the count of taxonomy terms organized by vocabularies, providing a clear overview of term usage within your Drupal site. Access the report at `/admin/reports/node-type-count/term`.

- **User-friendly Interface:** The module provides a user-friendly interface to view and interact with the entity count reports. The reports are presented in a tabular format for easy understanding and navigation.

- **Extensible and Customizable:** The Entity Count Analyzer module can be extended to support additional entity types and custom reports as per specific site requirements.

### Installation and Usage

1. Install the Entity Count Analyzer module like any other Drupal module.

2. Once installed, navigate to the "Reports" section in the Drupal administration menu.

3. Click on the "Entity Count Analyzer" sub-menu to access the available reports.

4. Choose the desired report (Node Count, User Count, Block Count, or Term Count) to view the corresponding entity counts.

5. Explore the reports, analyze the entity distribution, and leverage the insights to optimize your site's content, user management, and block configuration.
