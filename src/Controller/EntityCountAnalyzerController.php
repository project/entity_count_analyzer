<?php

namespace Drupal\entity_count_analyzer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for Entity Count Analyzer.
 */
class EntityCountAnalyzerController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityCountAnalyzerController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Displays the node count page.
   *
   * @return array
   *   Render array containing the node count page.
   */
  public function nodeCountPage() {
    $header = [
      $this->t('Content Type'),
      $this->t('Machine Name'),
      $this->t('Published'),
      $this->t('Unpublished'),
      $this->t('Total'),
    ];

    $rows = [];

    $nodeTypes = NodeType::loadMultiple();

    $publishedTotal = 0;
    $unpublishedTotal = 0;

    foreach ($nodeTypes as $nodeType) {
      $publishedCount = $this->getEntityCount('node', $nodeType->id(), 1);
      $unpublishedCount = $this->getEntityCount('node', $nodeType->id(), 0);
      $total = $publishedCount + $unpublishedCount;

      $rows[] = [
        'type' => $nodeType->label(),
        'machine_name' => $nodeType->id(),
        'published' => $publishedCount,
        'unpublished' => $unpublishedCount,
        'total' => $total,
      ];

      $publishedTotal += $publishedCount;
      $unpublishedTotal += $unpublishedCount;
    }

    $grandTotal = $publishedTotal + $unpublishedTotal;

    $rows[] = [
      'type' => $this->t('Total'),
      'machine_name' => '',
      'published' => $publishedTotal,
      'unpublished' => $unpublishedTotal,
      'total' => $grandTotal,
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Displays the user count page.
   *
   * @return array
   *   Render array containing the user count page.
   */
  public function userCountPage() {
    $header = [
      $this->t('Role Name'),
      $this->t('Number of Users'),
    ];

    $rows = [];

    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $totalUsers = 0;

    foreach ($roles as $role) {
      $userCount = $this->getUserCount($role->id());

      $rows[] = [
        'role_name' => $role->label(),
        'user_count' => $userCount,
      ];

      $totalUsers += $userCount;
    }

    // Add the total row.
    $rows[] = [
      'role_name' => $this->t('Total'),
      'user_count' => $totalUsers,
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Gets the user count for a specific role.
   *
   * @param string $roleId
   *   The ID of the role.
   *
   * @return int
   *   The count of users assigned to the role.
   */
  protected function getUserCount($roleId) {
    $query = $this->entityTypeManager->getStorage('user')->getQuery();
    $query->condition('roles', $roleId);
    $query->accessCheck(FALSE);
    return $query->count()->execute();
  }

  /**
   * Displays the block count page.
   *
   * @return array
   *   Render array containing the block count page.
   */
  public function blockCountPage() {
    $header = [
      $this->t('Block Type'),
      $this->t('Machine Name'),
      $this->t('Total'),
    ];

    $rows = [];

    $blockTypes = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple();

    $totalBlocks = 0;

    foreach ($blockTypes as $blockType) {
      $blockCount = $this->getBlockCount($blockType->id());

      $rows[] = [
        'type' => $blockType->label(),
        'machine_name' => $blockType->id(),
        'total' => $blockCount,
      ];

      $totalBlocks += $blockCount;
    }

    $rows[] = [
      'type' => $this->t('Total'),
      'machine_name' => '',
      'total' => $totalBlocks,
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Gets the block count for a specific block type.
   *
   * @param string $blockTypeId
   *   The ID of the block type.
   *
   * @return int
   *   The count of blocks matching the block type.
   */
  protected function getBlockCount($blockTypeId) {
    $query = $this->entityTypeManager->getStorage('block_content')->getQuery();
    $query->condition('type', $blockTypeId);
    $query->accessCheck(FALSE);
    return $query->count()->execute();
  }

  /**
   * Displays the term count page.
   *
   * @return array
   *   Render array containing the term count page.
   */
  public function termCountPage() {
    $header = [
      $this->t('Vocabulary'),
      $this->t('Machine Name'),
      $this->t('Total'),
    ];

    $rows = [];

    $vocabularies = Vocabulary::loadMultiple();

    $totalTerms = 0;

    foreach ($vocabularies as $vocabulary) {
      $termCount = $this->getEntityCount('taxonomy_term', $vocabulary->id());

      $rows[] = [
        'vocabulary' => $vocabulary->label(),
        'machine_name' => $vocabulary->id(),
        'total' => $termCount,
      ];

      $totalTerms += $termCount;
    }

    $rows[] = [
      'vocabulary' => $this->t('Total'),
      'machine_name' => '',
      'total' => $totalTerms,
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Counts of entities based on entity type, bundle, and optional status.
   *
   * @param string $entityType
   *   The entity type.
   * @param string|null $bundle
   *   The bundle (optional).
   * @param int|null $status
   *   The status (optional).
   *
   * @return int
   *   The count of entities.
   */
  protected function getEntityCount($entityType, $bundle = NULL, $status = NULL) {
    $query = $this->entityTypeManager->getStorage($entityType)->getQuery();
    // Set the condition based on the entity type.
    if ($bundle !== NULL) {
      $conditionField = ($entityType === 'taxonomy_term') ? 'vid' : 'type';
      $query->condition($conditionField, $bundle);
    }
    if ($status !== NULL) {
      $query->condition('status', $status);
    }
    $query->accessCheck(FALSE);
    return $query->count()->execute();
  }

}
